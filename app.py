from http.server import BaseHTTPRequestHandler, HTTPServer
from fn.randomName import getRandomName
import os

hostName = "0.0.0.0"
serverPort = 3000

class Server(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        if self.path == "/version":
            self.wfile.write(bytes(os.environ.get("COMMIT"), "utf-8"))
        else:
            self.wfile.write(bytes(getRandomName(), "utf-8"))

if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), Server)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")